@ECHO OFF

REM Aristocript - Gestor de sistemas Windows en Aulas.

REM Autor: Samuel Soto Paredes - 04/02/2021 samuelsotodam20@iescastelar.com

REM Para mas informacion sobre este Script leer la documentacion adjunto con este Script o la documentacion que puedes encontrar
REM publicada por su autor en https://docs.google.com/document/d/10GY_EoTJU2sYA20zn5d65onEnVjQ6vawTGUEITlAWjY/edit?usp=sharing

REM Con fines educativos, cualquier modificacon de este Script debe hacer mencion al autor original del mismo.

CLS

ECHO -------------------------------------------------------------
ECHO -                    -    IES CASTELAR   -                  -          
ECHO -                    ---------------------                  -
ECHO -                                                           -
ECHO - Script para la cracion de usuarios y grupos de las aluas  -
ECHO -                                                           -
ECHO -                                                           -
ECHO -------------------------------------------------------------
ECHO -                     Samuel Soto Paredes                   -
ECHO -------------------------------------------------------------
ECHO.
ECHO El prefijo usado sera: %1
ECHO.
ECHO La contrase�a por defecto sera: %2
ECHO.
ECHO El numero de usuarios sera de: %3
ECHO. 
ECHO El grupo al que perteneceran sera: %4
ECHO.

PAUSE

ECHO.

SET prefijo=%1
SET pass=%2
SET numusu=%3
SET grupo=%4

SET directorio=C:\%grupo%
SET profe=profesor_%grupo%

IF "%prefijo%"=="" GOTO ERR_PRE
IF "%pass%"=="" GOTO ERR_PASS
IF "%numusu%"=="" GOTO ERR_NUMUSU
IF "%grupo%"=="" GOTO ERR_GRU

IF EXIST "C:\%grupo%" GOTO FIN_MAL

FOR /L %%i IN (1, 1, %numusu%) DO (

	NET USER %prefijo%_%%i %pass% /ADD /LOGONPASSWORDCHG:YES>NUL

)

NET USER %profe% %pass% /ADD /LOGONPASSWORDCHG:YES>NUL
NET LOCALGROUP %grupo% /ADD>NUL

FOR /L %%i IN (1, 1, %numusu%) DO (

	NET LOCALGROUP %grupo% /ADD %prefijo%_%%i>NUL

)

NET LOCALGROUP %grupo% /ADD %profe%>NUL

IF ERRORLEVEL 2 GOTO ERROR_USUARIOREP

MKDIR %directorio%>NUL


FOR /L %%i IN (1, 1, %numusu%) DO (

	MKDIR %directorio%\%prefijo%_%%i>NUL

)

ICACLS %directorio% /INHERITANCE:R>NUL 
ICACLS %directorio% /GRANT:R %COMPUTERNAME%\%profe%:(OI)(CI)(F)>NUL
ICACLS %directorio% /GRANT %COMPUTERNAME%\%USERNAME%:(OI)(CI)(F)>NUL

FOR /L %%i IN (1, 1, %numusu%) DO (

	ICACLS %directorio% /GRANT %COMPUTERNAME%\%prefijo%_%%i:R>NUL
	ICACLS %directorio%\%prefijo%_%%i\ /GRANT %COMPUTERNAME%\%prefijo%_%%i:F /T>NUL

)

IF NOT ERRORLEVEL 0 GOTO FIN_MAL

MSG %USERNAME% El Script a funcionado correctamente
MSG %USERNAME% Se han creado los alumnos con el nombre "%prefijo%_X"
MSG %USERNAME% Se han creado el grupo "%grupo%"
MSG %USERNAME% Se ha creado el usuario profesor "%profe%"
MSG %USERNAME% Se han creado el directorio "%directorio%" con un subdirectorio para cada ususario del grupo
MSG %USERNAME% Se han modificado los permisos de acceso correctamente

CLS

SET /P respuesta=Desea compartir en red el directorio del grupo realizado? (S/n): 

IF "%respuesta%"=="s" (

NET SHARE %grupo%=%directorio%>NUL
IF ERRORLEVEL 0 MSG %USERNAME% El directorio ha sido compartido
IF NOT ERRORLEVEL 0 MSG %USERNAME% [ ERROR NOT SHARE ] - El directorio no ha sido compartido

)

CLS

SET /P copia=Desea generar un script que realice las mismas operaciones de forma automatica? (S/n): 

ICACLS %directorio% /remove %COMPUTERNAME%\%USERNAME%>NUL

IF "%copia%"=="s" GOTO COPIA

GOTO FIN

:ERROR_GROUPEXIST
CLS
MSG %USERNAME% [ERROR GROUPEXIST] - El grupo ya existe
GOTO FIN
:ERROR_USUARIOREP
CLS
MSG %USERNAME% [ERROR USUARIOREP] - El usuario ya pertenecia al grupo o ya existia
GOTO FIN
:ERR_PRE
CLS
MSG %USERNAME% [ERROR PARAM 1] - No has introducido el prefijo para los usuarios
GOTO FIN
:ERR_GRU
CLS
MSG %USERNAME% [ERROR PARAM 4] - No has introducido el grupo para los usuarios
GOTO FIN
:ERR_PASS
CLS
MSG %USERNAME% [ERROR PARAM 2] - No has introducido la contrase�a por defecto para los usuarios
GOTO FIN
:ERR_NUMUSU
CLS
MSG %USERNAME% [ERROR PARAM 3] - No has introducido el numero de usuarios
GOTO FIN
:FIN_MAL
MSG %USERNAME% [ERROR POR EXISTENCIA] - Esta calse ya ha sido creada
GOTO FIN

:COPIA
ECHO @ECHO OFF > gestor_aula_%grupo%.bat

ECHO REM Script Autogenerado por Aristocript para la gestion del grupo %grupo% >> gestor_aula_%grupo%.bat

ECHO REM Este Script fue generado %date% a las %time% >> gestor_aula_%grupo%.bat

ECHO SET directorio=C:\%grupo% >> gestor_aula_%grupo%.bat
ECHO SET profe=profesor_%grupo% >> gestor_aula_%grupo%.bat

ECHO SET prefijo=%prefijo% >> gestor_aula_%grupo%.bat
ECHO SET pass=%pass% >> gestor_aula_%grupo%.bat
ECHO SET numusu=%numusu% >> gestor_aula_%grupo%.bat
ECHO SET grupo=%grupo% >> gestor_aula_%grupo%.bat

SET z=%

SET a=%z%%a
SET c=%z%%c
SET i=%z%%i
SET t=%z%%t

ECHO SET x=%% >> gestor_aula_%grupo%.bat
ECHO SET y=%% >> gestor_aula_%grupo%.bat

ECHO SET a=%%%x%%%a >> gestor_aula_%grupo%.bat
ECHO SET c=%%%x%%%c >> gestor_aula_%grupo%.bat
ECHO SET i=%%%x%%%i >> gestor_aula_%grupo%.bat
ECHO SET t=%%%x%%%t >> gestor_aula_%grupo%.bat

ECHO IF EXIST %directorio% GOTO FIN_MAL >> gestor_aula_%grupo%.bat

ECHO FOR /L %%a%% IN (1, 1, %numusu%) DO ( >> gestor_aula_%grupo%.bat

ECHO	NET USER %prefijo%_%%a%% %pass% /ADD /LOGONPASSWORDCHG:YES>NUL >> gestor_aula_%grupo%.bat

ECHO ) >> gestor_aula_%grupo%.bat

ECHO NET USER %profe% %pass% /ADD /LOGONPASSWORDCHG:YES>NUL >> gestor_aula_%grupo%.bat
ECHO NET LOCALGROUP %grupo% /ADD>NUL >> gestor_aula_%grupo%.bat

ECHO FOR /L %%c%% IN (1, 1, %numusu%) DO ( >> gestor_aula_%grupo%.bat

ECHO 	NET LOCALGROUP %grupo% /ADD %prefijo%_%%c%%>NUL >> gestor_aula_%grupo%.bat

ECHO ) >> gestor_aula_%grupo%.bat

ECHO NET LOCALGROUP %grupo% /ADD %profe%>NUL >> gestor_aula_%grupo%.bat

ECHO IF ERRORLEVEL 2 GOTO ERROR_USUARIOREP >> gestor_aula_%grupo%.bat

ECHO MKDIR %directorio%>NUL >> gestor_aula_%grupo%.bat


ECHO FOR /L %%i%% IN (1, 1, %numusu%) DO ( >> gestor_aula_%grupo%.bat

ECHO 	MKDIR %directorio%\%prefijo%_%%i%%>NUL >> gestor_aula_%grupo%.bat

ECHO ) >> gestor_aula_%grupo%.bat

ECHO ICACLS %directorio% /INHERITANCE:R>NUL >> gestor_aula_%grupo%.bat
ECHO ICACLS %directorio% /GRANT:R %COMPUTERNAME%\%profe%:(OI)(CI)(F)>NUL >> gestor_aula_%grupo%.bat
ECHO ICACLS %directorio% /GRANT %COMPUTERNAME%\%USERNAME%:(OI)(CI)(F)>NUL >> gestor_aula_%grupo%.bat

ECHO FOR /L %%t%% IN (1, 1, %numusu%) DO ( >> gestor_aula_%grupo%.bat

ECHO 	ICACLS %directorio% /GRANT %COMPUTERNAME%\%prefijo%_%%t%%:R>NUL >> gestor_aula_%grupo%.bat
ECHO 	ICACLS %directorio%\%prefijo%_%%t%%\ /GRANT %COMPUTERNAME%\%prefijo%_%%t%%:F /T>NUL >> gestor_aula_%grupo%.bat

ECHO ) >> gestor_aula_%grupo%.bat

ECHO IF NOT ERRORLEVEL 0 GOTO FIN_MAL >> gestor_aula_%grupo%.bat

ECHO MSG %USERNAME% El Script a funcionado correctamente >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% Se han creado los alumnos con el nombre "%prefijo%_X" >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% Se han creado el grupo "%grupo%" >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% Se ha creado el usuario profesor "%profe%" >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% Se han creado el directorio "%directorio%" con un subdirectorio para cada ususario del grupo >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% Se han modificado los permisos de acceso correctamente >> gestor_aula_%grupo%.bat

ECHO CLS >> gestor_aula_%grupo%.bat

ECHO IF "%respuesta%"=="s" ( >> gestor_aula_%grupo%.bat

ECHO NET SHARE %grupo%=%directorio%>NUL >> gestor_aula_%grupo%.bat
ECHO IF ERRORLEVEL 0 MSG %USERNAME% El directorio ha sido compartido >> gestor_aula_%grupo%.bat
ECHO IF NOT ERRORLEVEL 0 MSG %USERNAME% [ ERROR NOT SHARE ] - El directorio no ha sido compartido >> gestor_aula_%grupo%.bat

ECHO ) >> gestor_aula_%grupo%.bat

ECHO ICACLS %directorio% /remove %COMPUTERNAME%\%USERNAME%>NUL >> gestor_aula_%grupo%.bat

ECHO CLS >> gestor_aula_%grupo%.bat

ECHO GOTO FIN >> gestor_aula_%grupo%.bat

ECHO SET "x=" >> gestor_aula_%grupo%.bat

ECHO :ERROR_GROUPEXIST >> gestor_aula_%grupo%.bat
ECHO CLS >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% [ERROR GROUPEXIST] - El grupo ya existe >> gestor_aula_%grupo%.bat
ECHO GOTO FIN >> gestor_aula_%grupo%.bat
ECHO :ERROR_USUARIOREP >> gestor_aula_%grupo%.bat
ECHO CLS >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% [ERROR USUARIOREP] - El usuario ya pertenecia al grupo o ya existia >> gestor_aula_%grupo%.bat
ECHO GOTO FIN >> gestor_aula_%grupo%.bat
ECHO :FIN_MAL >> gestor_aula_%grupo%.bat
ECHO MSG %USERNAME% [ERROR POR EXISTENCIA] - Esta calse ya ha sido creada >> gestor_aula_%grupo%.bat
ECHO :FIN >> gestor_aula_%grupo%.bat

:FIN
